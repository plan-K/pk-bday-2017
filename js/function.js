//variables





//functions
function init() {
	$("#submit").click(function(e) {
		e.preventDefault();
		//$("#form").addClass("off");

		var	superemail = $("input#superemail").val(),
			name = $("#name").text();

		console.log(superemail);
		console.log(name);
		if (validateEmail(superemail)){
			if( !(name === "") ){
				$.ajax({
					type: "POST",
					url: "process/include_db.php",
					data: {name: name, email: superemail},
					success: function(data){
						if($("body").data("lang") == "en") {
							$(".form").html("<p>The e-mail address has been saved.</p>");
						} else if($("body").data("lang") == "de") {
							$(".form").html("<p>Die E-Mail-Adresse wurde aufgenommen.</p>");
						} else if($("body").data("lang") == "lu") {
							$(".form").html("<p>D'E-mail Adress ass enregistréiert.</p>");
						} else {
							$(".form").html("<p>L'adresse e-mail a bien été enregistrée.</p>");
						}
					}
				});
			}
		}else{

		}
	});
}

function validateEmail(email) {
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

function init_mobile() {
	var attachFastClick = Origami.fastclick;
	attachFastClick(document.body);
}

function loaded() {

}