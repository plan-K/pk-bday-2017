<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title></title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="css/main.min.css?v=<?php echo uniqid(); ?>">
		<link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">

		<link rel="shortcut icon" href="/favicon.png">

		<script src="js/vendor/modernizr-3.3.1-respond-1.4.2.min.js"></script>
	</head>
	<body data-lang="<?php echo htmlspecialchars($_GET["l"]) ?>">
		<!--[if lt IE 8]>
			<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->
		<main>

			<div class="green_box"></div>

			<?php if($_GET["l"] == "en") { ?>
				<div class="center">
					<div class="width100">
						<div class="text1">
							<p>Hello<?php if($_GET["n"]) { ?> <span id="name"><?php echo htmlspecialchars($_GET["n"]) ?></span><?php } ?>, we have a message for you!</p>
						</div>
						<div class="video">
							<iframe width="100%" height="100%" id="agance_video_XX" src="https://www.youtube.com/embed/ynYtEy5ZQZE?autoplay=1&showinfo=0&controls=0&loop=1&playlist=RKblgBvjTrc&showinfo=0&rel=0" frameborder="0" allowfullscreen></iframe>
						</div>
						<?php
							if(htmlspecialchars($_GET["e"]) != "e"){
						?>
						<div class="text2">
							<p>To receive a little attention every year, it’s quite easy.</p>
						</div>
						<div class="form">
							<form id="bday_form" action="#" method="post">
								<input type="" name="superemail" id="superemail" placeholder="Your e-mail adress" >
								<button type="submit" id="submit">send</button>
							</form>
						</div>
						<?php } ?>
					</div>
				</div>
			<?php } else if($_GET["l"] == "de") { ?>
				<div class="center">
					<div class="width100">
						<div class="text1">
							<p>Hallo<?php if($_GET["n"]) { ?> <span id="name"><?php echo htmlspecialchars($_GET["n"]) ?></span><?php } ?>, wir haben eine Mitteilung für <?php if(htmlspecialchars($_GET["s"]) == "v"){ echo "Sie";}else{echo "dich";} ?>!</p>
						</div>
						<div class="video">
							<?php
								if(htmlspecialchars($_GET["s"]) == "v"){
							?>
							<iframe width="100%" height="100%" id="agance_video_XX" src="https://www.youtube.com/embed/YHbrJA8ZL8A?autoplay=1&showinfo=0&controls=0&loop=1&playlist=RUrrUk0OlMc&showinfo=0&rel=0" frameborder="0" allowfullscreen></iframe>
							<?php
								} else {
							?>
							<iframe width="100%" height="100%" id="agance_video_XX" src="https://www.youtube.com/embed/zfMbVF1xaGs?autoplay=1&showinfo=0&controls=0&loop=1&playlist=xHl9Hw2IrVs&showinfo=0&rel=0" frameborder="0" allowfullscreen></iframe>
							<?php } ?>
						</div>
						<?php
							if(htmlspecialchars($_GET["e"]) != "e"){
						?>
						<div class="text2">
							<?php if(htmlspecialchars($_GET["s"]) == "v"){ ?>
								<p>Wenn Sie jedes Jahr eine kleine Geste von uns für Ihren Geburtstag bekommen wollen, nichts einfacher als das.</p>
							<?php } else { ?>
								<p>Wenn du jedes Jahr eine kleine Geste von uns für deinen Geburtstag bekommen willst, nichts einfacher als das.</p>
							<?php } ?>
						</div>
						<div class="form">
							<form id="bday_form" action="#" method="post">
								<input type="" name="superemail" id="superemail" placeholder="<?php if(htmlspecialchars($_GET["s"]) == "v"){ echo "Ihre";}else{echo "Deine";} ?> E-Mail-Adresse" >
								<button type="submit" id="submit">schicken</button>
							</form>
						</div>
						<?php } ?>
					</div>
				</div>
			<?php } else if($_GET["l"] == "lu") { ?>
				<div class="center">
					<div class="width100">
						<div class="text1">
							<p>Hallo<?php if($_GET["n"]) { ?> <span id="name"><?php echo htmlspecialchars($_GET["n"]) ?></span><?php } ?>, mir hunn ee klenge Message fir <?php if(htmlspecialchars($_GET["s"]) == "v"){ echo "Iech";}else{echo "dech";} ?>!</p>
						</div>
						<div class="video">
							<?php
								if(htmlspecialchars($_GET["s"]) == "v"){
							?>
							<iframe width="100%" height="100%" id="agance_video_XX" src="https://www.youtube.com/embed/fgu4e8EkrnY?autoplay=1&showinfo=0&controls=0&loop=1&playlist=f4WkluphJXw&showinfo=0&rel=0" frameborder="0" allowfullscreen></iframe>
							<?php
								} else {
							?>
							<iframe width="100%" height="100%" id="agance_video_XX" src="https://www.youtube.com/embed/m2qCJImgOsU?autoplay=1&showinfo=0&controls=0&loop=1&playlist=Wu47P5rjJVk&showinfo=0&rel=0" frameborder="0" allowfullscreen></iframe>
							<?php } ?>
						</div>
						<?php
							if(htmlspecialchars($_GET["e"]) != "e"){
						?>
						<div class="text2">
							<?php if(htmlspecialchars($_GET["s"]) == "v"){ ?>
								<p>Wann Dir all Joer wëllt vun ons eng Klengegkeet geschéckt kréien, da gitt ons w.e.g. Är E-mail Adress.</p>
							<?php } else { ?>
								<p>Wann s de all Joer wëlls vun ons eng Klengegkeet geschéckt kréien, da gëff ons w.e.g. deng E-mail Adress.</p>
							<?php } ?>
						</div>
						<div class="form">
							<form id="bday_form" action="#" method="post">
								<input type="" name="superemail" id="superemail" placeholder="<?php if(htmlspecialchars($_GET["s"]) == "v"){ echo "Är";}else{echo "Deng";} ?> E-Mail Adress" >
								<button type="submit" id="submit">schécken</button>
							</form>
						</div>
						<?php } ?>
					</div>
				</div>
			<?php } else { ?>
				<div class="center">
					<div class="width100">
						<div class="text1">
							<p>Hello<?php if($_GET["n"]) { ?> <span id="name"><?php echo htmlspecialchars($_GET["n"]) ?></span><?php } ?>, nous avons un petit message pour <?php if(htmlspecialchars($_GET["s"]) == "v"){ echo "vous";}else{echo "toi";} ?> !</p>
						</div>
						<div class="video">
							<?php
								if(htmlspecialchars($_GET["s"]) == "v"){
							?>
							<iframe width="100%" height="100%" id="agance_video_XX" src="https://www.youtube.com/embed/O358z2dndOk?autoplay=1&showinfo=0&controls=0&loop=1&playlist=N1Poqx4eIfA&showinfo=0&rel=0" frameborder="0" allowfullscreen></iframe>
							<?php
								} else {
							?>
							<iframe width="100%" height="100%" id="agance_video_XX" src="https://www.youtube.com/embed/vVZafVtQf8g?autoplay=1&showinfo=0&controls=0&loop=1&playlist=jmRTGaGzLjs&showinfo=0&rel=0" frameborder="0" allowfullscreen></iframe>
							<?php } ?>
						</div>
						<?php
							if(htmlspecialchars($_GET["e"]) != "e"){
						?>
						<div class="text2">
							<p>Pour recevoir tous les ans nos petites attentions pour <?php if(htmlspecialchars($_GET["s"]) == "v"){ echo "votre";}else{echo "ton";} ?> anniversaire, rien de plus simple.</p>
						</div>
						<div class="form">
							<form id="bday_form" action="#" method="post">
								<input type="" name="superemail" id="superemail" placeholder="<?php if(htmlspecialchars($_GET["s"]) == "v"){ echo "Votre";}else{echo "Ton";} ?> adresse e-mail" >
								<button type="submit" id="submit">envoyer</button>
							</form>
						</div>
						<?php } ?>
					</div>
				</div>
			<?php } ?>

			<div class="logo">
				<a id="logo_pk" href="http://plan-k.lu/" target="_blank"></a>
			</div>

		</main>



<!--
			<div class="logo">
				<a id="logo_pk" href="http://plan-k.lu/" target="_blank"></a>
			</div>
			<div class="center">
				<div class="width100">
					<div class="text">
						<p>Hello <span>XXXXXXXXX</span></p>
						<hr>
						<p>Nous avons un petit message pour toi !</p>
					</div>
					<div class="video">
						<iframe width="100%" height="100%" id="agance_video_XX" src="https://www.youtube.com/embed/O2aPmN9jzfI?autoplay=1&showinfo=0&controls=0&loop=1&playlist=O2aPmN9jzfI" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="text">
						<p>Et si tu veux recevoir tous les ans<br> nos petites attentions pour ton anniversaire...</p>
					</div>
					<div class="form">
						<form name="myForm" action="#" method="post">
							<input type="email" name="email" placeholder="Ton adresse e-mail" >
							<button type="submit"></button>
						</form>
					</div>
				</div>
			</div>
-->




		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.0.0.min.js"><\/script>')</script>

		<script src="js/vendor/fastclick.js"></script>

		<script src="js/function.min.js"></script>
		<script src="js/main.min.js"></script>

		<!--[if (gte IE 6)&(lte IE 8)]>
		  <script type="text/javascript" src="js/vendor/selectivizr-min.js"></script>
		<![endif]-->
	</body>
</html>
